-- 6. Вывести долю рейсов, о которых существует информация в базе о проданных билетах {tickets_not_null_ratio}. Доля - вещественное число от 0 до 1.
-- Решение через CTE

WITH
    sold_tickets_flights AS (
        SELECT
            COUNT(DISTINCT flight_id) AS cnt
        FROM ticket_flights
    ),

    all_flights AS (
        SELECT
            COUNT(DISTINCT flight_id) AS cnt
        FROM flights
    )

select
    s.cnt::decimal / a.cnt as ratio
from sold_tickets_flights s, all_flights a;

-- Фильтр по дате, когда тип scheduled_departure - это timestamp. Не надо никаких сравнений через between

select
    *
from flights
where scheduled_departure::date = '2017-08-01';

-- То же самое, другой способ

select
    *
from flights
where CAST(scheduled_departure AS DATE) = '2017-08-01';

-- 9. Для 15.08.2017 вывести, сколько пассажиров не путешествовали последние 7 дней, но путешествовали ранее {cnt}.

WITH
     fligth_statistics_by_passenger AS (
         SELECT
            passenger_id,
            count(f.scheduled_departure::DATE BETWEEN DATE('2017-08-15') - INTERVAL '6 DAY' AND DATE('2017-08-15') OR NULL) cnt_7d,
            count(f.scheduled_departure::DATE < DATE('2017-08-15') - INTERVAL '6 DAY' OR NULL) cnt_before_7d
        FROM tickets t
        LEFT JOIN ticket_flights tf ON t.ticket_no = tf.ticket_no
        LEFT JOIN flights f ON tf.flight_id = f.flight_id
        GROUP BY passenger_id
     )

SELECT
    COUNT(passenger_id) AS cnt
FROM fligth_statistics_by_passenger
WHERE cnt_7d = 0 AND cnt_before_7d > 0;

-- 10. Bonus task! Вывести запланированные даты полётов в промежутке от первой до последней дат запланированных полётов, в которые не летали самолёты модели Sukhoi Superjet-100.

WITH
    all_dates AS (
        SELECT DISTINCT
            scheduled_departure::date
        FROM flights
    ),

    superjet_dates AS (
        SELECT DISTINCT
            scheduled_departure::date
        FROM flights f
        INNER JOIN aircrafts a ON f.aircraft_code = a.aircraft_code
        WHERE a.model = 'Sukhoi Superjet-100'
    )

SELECT
    *
FROM all_dates a
LEFT JOIN superjet_dates s ON a.scheduled_departure = s.scheduled_departure
WHERE s.scheduled_departure IS NULL;

-- Вывести самолёты, для которых существует хотя бы один аэропорт, куда летают только самолёты этой модели. Решить с помощью EXISTS и NOT EXISTS.

select
    *
from aircrafts ar
where exists(
    select
        *
    from airports ap
    where not exists(
        select
            *
        from flights f
        where f.arrival_airport = ap.airport_code
            and ar.aircraft_code <> f.aircraft_code
    )
)
order by ar.aircraft_code;

-- Аналогично, но более эффективно без использования EXISTS и NOT EXISTS

with
    airports_filtering AS (
        select
            arrival_airport,
            count(distinct aircraft_code) unique_aircrafts
        from flights
        group by arrival_airport
        having count(distinct aircraft_code) = 1
    )

select distinct
    a.aircraft_code,
    a.model,
    a.range
from flights f
left join aircrafts a on f.aircraft_code = a.aircraft_code
inner join airports_filtering af on f.arrival_airport = af.arrival_airport
order by a.aircraft_code;

-- Вывести информацию обо всех рейсах, добавить колонку, где будет написано общее кол-во полётов в БД
-- Используются оконные функции

select
    *,
    COUNT(*) OVER () all_flights_cnt
from flights;

-- Вывести информацию обо всех рейсах, добавить колонку, где будет написано общее кол-во полётов в БД из аэропорта вылета

select
    *,
    COUNT(*) OVER (PARTITION BY departure_airport) all_flights_cnt
from flights;

-- Вывести информацию обо всех рейсах, добавить колонку, где будет написано общее кол-во полётов в БД из аэропорта вылета ДО момента вылета

select
    *,
    COUNT(*) OVER (PARTITION BY departure_airport ORDER BY actual_departure) all_flights_cnt
from flights;
