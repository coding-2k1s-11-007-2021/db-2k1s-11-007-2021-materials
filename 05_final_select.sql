-- 4. Для каждого рейса {*} вывести поля с дополнительной информацией (Note! Во всех пунктах сортировать по фактическому времени вылета и номеру рейса во избежание ситуации “2 рейса вылетело одновременно”. Избегать NULL-ов в выводе, заменить на 0, познакомьтесь с функцией COALESCE):
-- - Средняя стоимость одного билета на предыдущие 10 рейсов из текущего аэропорта отправления (не включая этот рейс) {prev_10_amount_sum}
-- - Средняя стоимость суммы билетов на рейс из следующих 10 рейсов из текущего аэропорта отправления (не включая этот рейс) {next_10_amount_sum}. Средняя стоимость суммы билетов на рейс - это для каждого рейса суммируете стоимость билетов, затем считаете, сколько в среднем один рейс принёс денег.

with
    flights_additional_tickets as (
        select
            f.flight_id,
            f.departure_airport,
            f.actual_departure,
            sum(tf.amount) sum_amount,
            count(tf.ticket_no) count_tickets
        from flights f
        left join ticket_flights tf on f.flight_id = tf.flight_id
        group by f.flight_id, f.departure_airport, f.actual_departure
    )

select
    flight_id,
    departure_airport,
    coalesce(sum(sum_amount) over (partition by departure_airport, flight_id order by actual_departure rows between 10 preceding and 1 preceding), 0) /
        coalesce(sum(count_tickets) over (partition by departure_airport, flight_id order by actual_departure rows between 10 preceding and 1 preceding), 1) prev_10_amount_sum,
    coalesce(avg(sum_amount) over (partition by departure_airport, flight_id order by actual_departure rows between 1 following and 10 following), 0) as next_10_amount_sum
from flights_additional_tickets;

-- Вывести самолёты, рядом добавить колонку с порядковым номером по увеличению дальности полёта (рядом с самой малой дальностью полёта будет написано 1, следующий по дальности 2 и т.д.)
-- Изучить разницу работы функций row_number(), rank(), dense_rank() - см. сообщение в тг.

select
    *,
    row_number() over (order by range)
from aircrafts;

-- Рядом с каждым самолётом отобразить, является ли он в топ 25% по дальности полёта (вывести true/false).

select
    *,
    rank() over (order by range desc)::decimal / count(*) over () <= 0.25 as is_top_25
from aircrafts;

-- Работа оператора UNION ALL (UNION DISTINCT) - объединяем множества одной структуры

select
    status,
    count(*) cnt
from flights
group by status

union all

select
    'all',
    count(*) cnt
from flights

union all

select
    'On Time',
    518;

-- Посчитать факториалы для чисел 1-10 - рекурсивные CTE. Подробнее: https://habr.com/ru/post/269497/

WITH RECURSIVE r AS (
    -- стартовая часть рекурсии (т.н. "anchor")
    SELECT
        1 AS i,
        1 AS factorial

    UNION

    -- рекурсивная часть
    SELECT
        i+1 AS i,
        factorial * (i+1) as factorial
    FROM r
    WHERE i < 10
)
SELECT * FROM r;
