-- 1. Вывести рейсы с запланированной датой прилёта 01.08.2017 в аэропорты Москвы со статусом Arrived. Для каждого рейса вывести поле, говорящее, заканчивался ли этот полёт в первой половине дня (включая ровно 12:00) или во второй (вывести значение “before midday” или “after midday”). {flight_id, flight_no, departure_airport, arrival_airport, scheduled_departure, scheduled_arrival, status, before_after_midday}

select
    flight_id,
    flight_no,
    departure_airport,
    arrival_airport,
    scheduled_departure,
    scheduled_arrival,
    status,
    case
        when scheduled_arrival::time <= '12:00' then 'before midday'
        else 'after midday'
    end as before_after_midday
from flights f
left join airports a on f.arrival_airport = a.airport_code
where
    scheduled_arrival::date = '2017-08-01'
    and status = 'Arrived'
    and a.city = 'Moscow';

-- 2. Для каждого самолёта определить процент мест класса Business по отношению к общему числу мест в самолёте, вывести топ 5 по убыванию этого процента. {aircraft_code, model, business_cnt}

select
    a.aircraft_code,
    a.model,
    count(fare_conditions = 'Business' or NULL) * 100.0 / count(*) as business_cnt
from aircrafts a
left join seats s on a.aircraft_code = s.aircraft_code
group by a.aircraft_code, a.model
order by business_cnt desc
limit 5;

-- 3. Для каждой модели самолёта определить среднесуточное по запланированной дате вылета число уникальных пассажиров, вывести топ 5. Напомню, что пассажир однозначно идентифицируется только полем passenger_id. {aircraft_code, model, cnt}

with
    unique_passengers_per_date_and_aircraft as (
        select
            a.aircraft_code,
            a.model,
            f.scheduled_departure::date as scheduled_departure_date,
            count(distinct t.passenger_id) as unique_passenger_id_cnt
        from aircrafts a
        left join flights f on a.aircraft_code = f.aircraft_code
        left join ticket_flights tf on f.flight_id = tf.flight_id
        left join tickets t on tf.ticket_no = t.ticket_no
        group by
            a.aircraft_code,
            a.model,
            scheduled_departure_date
    )

select
    aircraft_code,
    model,
    avg(unique_passenger_id_cnt) as cnt
from unique_passengers_per_date_and_aircraft
group by aircraft_code, model
order by cnt desc
limit 5;

-- 4. Для каждого рейса определить были ли рейсы со статусом Cancelled из текущего аэропорта отправления в 10 рейсах ранее (отсортируйте по scheduled_departure и flight_id). {flight_id, flight_no, departure_airport, scheduled_departure, has_prev10_cancelled}

select
    flight_id,
    flight_no,
    departure_airport,
    scheduled_departure,
    count(status = 'Cancelled' or null) over (partition by departure_airport order by scheduled_departure, flight_id rows between 10 preceding and 1 preceding) > 0 as has_prev10_cancelled
from flights;

-- 5. Определить процент пассажиров, у которых после какого-либо рейса, связанного с Москвой, был хотя бы один региональный рейс (никак не связан с Москвой). Если у человека вообще не было связанных с Москвой рейсов, учитывать его в процент не надо. {percent}

with
     first_moscow_flights as (
        select
            passenger_id,
            min(scheduled_departure) as moscow_first_flight_dt
        from tickets t
        left join ticket_flights tf on t.ticket_no = tf.ticket_no
        left join flights f on f.flight_id = tf.flight_id
        left join airports a1 on f.arrival_airport = a1.airport_code
        left join airports a2 on f.departure_airport = a2.airport_code
        where a1.city = 'Moscow' or a2.city = 'Moscow'
        group by passenger_id
        having min(scheduled_departure) is not null
     ),

    last_non_moscow_flights as (
        select
            passenger_id,
            max(scheduled_departure) as non_moscow_last_flight_dt
        from tickets t
        left join ticket_flights tf on t.ticket_no = tf.ticket_no
        left join flights f on f.flight_id = tf.flight_id
        left join airports a1 on f.arrival_airport = a1.airport_code
        left join airports a2 on f.departure_airport = a2.airport_code
        where a1.city <> 'Moscow' and a2.city <> 'Moscow'
        group by passenger_id
        having max(scheduled_departure) is not null
    ),

    non_moscow_after_moscow_flights as (
        select
            count(distinct m.passenger_id) cnt
        from first_moscow_flights m
        left join last_non_moscow_flights n on m.passenger_id = n.passenger_id
        where moscow_first_flight_dt < non_moscow_last_flight_dt
    )

select
    cnt * 100.0 / count(distinct passenger_id) as percent
from tickets
left join non_moscow_after_moscow_flights on 1=1
group by cnt;
