-- 1. Вывести рейсы с запланированной датой вылета 01.09.2017 из аэропортов Москвы самолётами Airbus. Для каждого рейса вывести поле, говорящее, начинался ли этот полёт в первой половине дня (включая ровно 12:00) или во второй (вывести значение “before midday” или “after midday”). {flight_id, flight_no, departure_airport, arrival_airport, scheduled_departure, scheduled_arrival, status, before_after_midday}

select
    flight_id,
    flight_no,
    departure_airport,
    arrival_airport,
    scheduled_departure,
    scheduled_arrival,
    status,
    case
        when scheduled_departure::time <= '12:00' then 'before midday'
        else 'after midday'
    end as before_after_midday
from flights f
left join airports ap on f.departure_airport = ap.airport_code
left join aircrafts ac on f.aircraft_code = ac.aircraft_code
where
    scheduled_departure::date = '2017-09-01'
    and ac.model LIKE 'Airbus%'
    and ap.city = 'Moscow';

-- 2. Для каждой запланированной даты вылета определить суммарную стоимость проданных билетов на рейсы со статусом Arrived. Результаты отсортировать по возрастанию даты. {flight_date, total_sum}

select
    f.scheduled_departure::date as flight_date,
    sum(tf.amount) as total_sum
from flights f
left join ticket_flights tf on f.flight_id = tf.flight_id
where status = 'Arrived'
group by flight_date
order by flight_date;

-- 3. Определить среднесуточное по запланированной дате вылета число уникальных пассажиров для каждого города отправления, вывести топ 5. Напомню, что пассажир однозначно идентифицируется только полем passenger_id. {city, cnt}

with
    unique_passengers_per_date_and_city as (
        select
            a.city,
            f.scheduled_departure::date as scheduled_departure_date,
            count(distinct t.passenger_id) as unique_passenger_id_cnt
        from airports a
        left join flights f on a.airport_code = f.departure_airport
        left join ticket_flights tf on f.flight_id = tf.flight_id
        left join tickets t on tf.ticket_no = t.ticket_no
        group by
            a.city,
            scheduled_departure_date
    )

select
    city,
    avg(unique_passenger_id_cnt) as cnt
from unique_passengers_per_date_and_city
group by city
order by cnt desc
limit 5;

-- 4. Для каждого рейса определить, входит ли он в 10% самых прибыльных рейсов из текущего аэропорта отправления. {flight_id, flight_no, departure_airport, scheduled_departure, is_top10}

with
    flights_amount_info as (
        select
            f.flight_id,
            f.flight_no,
            f.departure_airport,
            f.scheduled_departure,
            coalesce(sum(tf.amount), 0) as flight_amount
        from flights f
        left join ticket_flights tf on f.flight_id = tf.flight_id
        group by
            f.flight_id,
            f.flight_no,
            f.departure_airport,
            f.scheduled_departure
    )

select
    flight_id,
    flight_no,
    departure_airport,
    scheduled_departure,
    flight_amount,
    rank() over (partition by departure_airport order by flight_amount desc)::real / count(*) over (partition by departure_airport) <= 0.1 as is_top10
from flights_amount_info;

-- 5. Посчитать, сколько процентов пассажиров из летавших в период 01.08.2017 по 14.08.2017 летали ещё и в период с 15.08.2017 по 28.08.2017. Т.е. берёте людей, летавших с 01.08.2017 по 14.08.2017, и считаете, сколько из них летали с 15.08.2017 по 28.08.2017. Напомню, что пассажир однозначно идентифицируется только полем passenger_id. {percent}

with
    passengers_1_14 as (
        select distinct
            passenger_id
        from tickets t
        left join ticket_flights tf on t.ticket_no = tf.ticket_no
        left join flights f on tf.flight_id = f.flight_id
        where
            scheduled_departure::date between '2017-08-01' and '2017-08-14'
    ),

    passengers_15_28 as (
        select distinct
            passenger_id
        from tickets t
        left join ticket_flights tf on t.ticket_no = tf.ticket_no
        left join flights f on tf.flight_id = f.flight_id
        where
            scheduled_departure::date between '2017-08-15' and '2017-08-28'
    )

select
    count(p2.passenger_id) * 100.0 / count(p1.passenger_id) as percent
from passengers_1_14 p1
left join passengers_15_28 p2 on p1.passenger_id = p2.passenger_id;
